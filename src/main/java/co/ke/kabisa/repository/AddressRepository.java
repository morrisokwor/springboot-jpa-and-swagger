package co.ke.kabisa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ke.kabisa.entities.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long>{

}
