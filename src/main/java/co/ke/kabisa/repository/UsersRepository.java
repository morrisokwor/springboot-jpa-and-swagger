package co.ke.kabisa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ke.kabisa.entities.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long>{

	boolean existsByEmail(String email);

	boolean existsByPhoneNo(String phone);

}
