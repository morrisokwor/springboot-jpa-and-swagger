package co.ke.kabisa;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import co.ke.kabisa.entities.Role;
import co.ke.kabisa.repository.RoleRepository;

@SpringBootApplication
public class KabisaApplication {

	@Autowired
	RoleRepository roleRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(KabisaApplication.class, args);
	}

	@PostConstruct
	public void createRole() {
//		Role role = new Role("User", "USR");
		Role role = new Role();
		role.setCode("USR");
		role.setName("User");
		roleRepository.save(role);
	}
}

