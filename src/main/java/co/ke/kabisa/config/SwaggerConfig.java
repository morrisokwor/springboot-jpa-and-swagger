package co.ke.kabisa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class SwaggerConfig {

	@Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI().info(new Info().title("Booking Agency API").version("V 0.0.1")
				.description("Booking Agency API").termsOfService("http://primecloudsolutions.co.ke")
				.license(new License().name("Booking Agency API").url("")));
	}
}
