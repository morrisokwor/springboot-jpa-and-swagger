package co.ke.kabisa.models;

public class AddressModel {

	private Long addressId;
	private String postalAddres;
	private String name;
	private Long userId;
	public Long getAddressId() {
		return addressId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}
	public String getPostalAddres() {
		return postalAddres;
	}
	public void setPostalAddres(String postalAddres) {
		this.postalAddres = postalAddres;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
