package co.ke.kabisa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.ke.kabisa.models.SignupModel;
import co.ke.kabisa.models.UserModel;
import co.ke.kabisa.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("/create")
	public ResponseEntity<?> createuser(@RequestBody SignupModel signupModel) {

		if (userService.existsByEmail(signupModel.getEmail())) {
			return ResponseEntity.badRequest().body("Eamil Already Exist");
		}
		if (userService.existsByPhoneNo(signupModel.getEmail())) {
			return ResponseEntity.badRequest().body("Phone Number Already Exist");
		}

		return ResponseEntity.ok(userService.createUser(signupModel));
	}

	@PutMapping("/update")
	public ResponseEntity<?> updateUser(@RequestBody UserModel usrModel) {
		if (!userService.existsById(usrModel.getUserId())) {
			return ResponseEntity.badRequest().body("User Does not exist");
		}

		return ResponseEntity.ok(userService.upadteUser(usrModel));
	}
}
