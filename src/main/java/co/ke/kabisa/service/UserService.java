package co.ke.kabisa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.ke.kabisa.entities.Address;
import co.ke.kabisa.entities.Role;
import co.ke.kabisa.entities.Users;
import co.ke.kabisa.models.SignupModel;
import co.ke.kabisa.models.UserModel;
import co.ke.kabisa.repository.RoleRepository;
import co.ke.kabisa.repository.UsersRepository;

@Service
public class UserService {

	@Autowired
	UsersRepository usersRepository;

	@Autowired
	RoleRepository roleRepository;

	public Users createUser(SignupModel signupModel) {

		Role role = roleRepository.findById(signupModel.getRoleId()).get();

		Users user = new Users();
		user.setEmail(signupModel.getEmail());
		user.setFirstName(signupModel.getFirstName());
		user.setLastName(signupModel.getLastName());
		user.setOtherNames(signupModel.getOtherNames());
		user.setPassword(signupModel.getPassword());
		user.setPhoneNo(signupModel.getPhoneNo());
		user.setRole(role);

		Users savedUser = usersRepository.save(user);

		if (savedUser != null) {
			Address address = new Address();
			address.setName(signupModel.getName());
			address.setPostalAddres(signupModel.getPostalAddres());
			address.setUser(savedUser);
		}
		return savedUser;

	}

	public Users upadteUser(UserModel userModel) {

		Users user = usersRepository.findById(userModel.getUserId()).get();
		user.setEmail(userModel.getEmail());
		user.setFirstName(userModel.getFirstName());
		user.setLastName(userModel.getLastName());
		user.setOtherNames(userModel.getOtherNames());
		user.setPassword(userModel.getPassword());
		user.setPhoneNo(userModel.getPhoneNo());
		user.setRole(roleRepository.findById(userModel.getRoleId()).get());

		return usersRepository.save(user);

	}

	public boolean existsByEmail(String email) {
		// TODO Auto-generated method stub
		return usersRepository.existsByEmail(email);
	}

	public boolean existsByPhoneNo(String phone) {
		// TODO Auto-generated method stub
		return usersRepository.existsByPhoneNo(phone);
	}

	public boolean existsById(Long userId) {
		// TODO Auto-generated method stub
		return usersRepository.existsById(userId);
	}

}
