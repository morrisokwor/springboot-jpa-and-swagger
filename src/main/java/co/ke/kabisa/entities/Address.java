/**
 * 
 */
package co.ke.kabisa.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author okworo
 *
 */
@Entity
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long addressId;
	private String postalAddres;
	private String name;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private Users user;

	public Address() {
		// TODO Auto-generated constructor stub
	}

	public Address(String postalAddres, String name, Users user) {
		this.postalAddres = postalAddres;
		this.name = name;
		this.user = user;
	}



	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public String getPostalAddres() {
		return postalAddres;
	}

	public void setPostalAddres(String postalAddres) {
		this.postalAddres = postalAddres;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

}
